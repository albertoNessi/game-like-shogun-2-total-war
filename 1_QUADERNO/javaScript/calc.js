// prelevare i due operatori inseriti
var op1 = document.getElementById("operatore1");
var op2 = document.getElementById("operatore2");

// appenderli al div di destra
var div_operaz_Effettuate = document.getElementById("calc_Dx");

// somma
function somma(){
    var risultato = parseFloat(op1.value) + parseFloat(op2.value);
    // se si clicca il pulsante "=" senza aver messo i valori, mostrare questo messaggio
    if(op1.value == "" && op2.value == "" ){
        op1.style.border = "3px solid red";
        op1.placeholder = "inserisci dei valori";
        op2.style.border = "3px solid red";
        op2.placeholder = "inserisci dei valori";
    }else{
        op1.style.border = "3px solid black";
        op1.placeholder = "";
        op2.style.border = "3px solid black";
        op2.placeholder = "";
        // titolo_OpEffettuate.innerHTML = "Operazioni effettuate";
        // creo il messaggio del risultato
        var paragrafo_OpEffSomma = document.createElement("p");
        div_operaz_Effettuate.appendChild(paragrafo_OpEffSomma);
        paragrafo_OpEffSomma.id = "messaggioSomma";
        paragrafo_OpEffSomma.innerHTML = "Somma: " + op1.value + " + " + op2.value + " = " + risultato + "<br>";
        op1.value = "";
        op2.value = "";
    }
}
// sottrazione
var op3 = document.getElementById("operatore3");
var op4 = document.getElementById("operatore4");
function sottrazione(){
    var risultatoSottrazione = parseFloat(op3.value) - parseFloat(op4.value);
    // controllo
    if(op3.value == "" && op4.value == "" ){
        op3.style.border = "3px solid red";
        op4.placeholder = "inserisci dei valori";
        op3.style.border = "3px solid red";
        op4.placeholder = "inserisci dei valori";
    }else{
        op1.style.border = "3px solid black";
        op1.placeholder = "";
        op2.style.border = "3px solid black";
        op2.placeholder = "";
        var paragrafo_OpEffDifferenza = document.createElement("p");
        div_operaz_Effettuate.appendChild(paragrafo_OpEffDifferenza);
        paragrafo_OpEffDifferenza.id = "messaggioDifferenza";
        paragrafo_OpEffDifferenza.innerHTML = "Sottrazione: " + op3.value + " - " + op4.value + " = " + risultatoSottrazione + "<br>";
        op3.value = "";
        op4.value = "";
    }
}
// divisione
var op5 = document.getElementById("operatore5");
var op6 = document.getElementById("operatore6");
function divisione(){
    var risultatoDivisione = parseFloat(op5.value) / parseFloat(op6.value);
    // controllo
    if(op5.value == "" && op6.value == ""){
        op5.style.border = "3px solid red";
        op6.placeholder = "inserisci dei valori";
        op5.style.border = "3px solid red";
        op6.placeholder = "inserisci dei valori";
    }else{
        op5.style.border = "3px solid black";
        op6.placeholder = "";
        op5.style.border = "3px solid black";
        op6.placeholder = "";
        var paragrafo_OpEffDivisione = document.createElement("p");
        div_operaz_Effettuate.appendChild(paragrafo_OpEffDivisione);
        paragrafo_OpEffDivisione.id = "messaggioDifferenza";
        paragrafo_OpEffDivisione.innerHTML = "Divisione: " + op5.value + " / " + op6.value + " = " + risultatoDivisione + "<br>";
        op5.value = "";
        op6.value = "";
    }
}
// moltiplicazione
var op7 = document.getElementById("operatore7");
var op8 = document.getElementById("operatore8");
function moltiplicazione(){
    var risultatoMoltiplicazione = parseFloat(op7.value) * parseFloat(op8.value);
    // controllo
    if(op7.value == "" && op8.value == ""){
        op7.style.border = "3px solid red";
        op8.placeholder = "inserisci dei valori";
        op7.style.border = "3px solid red";
        op8.placeholder = "inserisci dei valori";
    }else{
        op7.style.border = "3px solid black";
        op8.placeholder = "";
        op7.style.border = "3px solid black";
        op8.placeholder = "";
        var paragrafo_OpEffMoltiplicazione = document.createElement("p");
        div_operaz_Effettuate.appendChild(paragrafo_OpEffMoltiplicazione);
        paragrafo_OpEffMoltiplicazione.id = "messaggioDifferenza";
        paragrafo_OpEffMoltiplicazione.innerHTML = "Moltiplicazione: " + op7.value + " * " + op8.value + " = " + risultatoMoltiplicazione + "<br>";
        op7.value = "";
        op8.value = "";
    }
}
// function reset(){
//     for(i=1; i<9; i++){
//         var op = document.getElementById("operatore" + i);
//         op.value = "";
//     }
// }