var numeroArmata = ARMATA.UNITA;

var messaggioMuroDistrutto = "Signore, abbiamo distrutto le mura! Adesso possiamo attaccare l'esercito avversario!";
var messaggioVittoria = "Signore, abbiamo conquistato l'insediamento!";
var messaggioSconfitta = "Signore, abbiamo perso la battaglia!! Gli uomini si stanno ritirando!!"; 
var divClanArmata = document.getElementById("divClanArmata");
var mostraQui = document.getElementById("messaggi");

var bgMura = "img/imgGioco/attacoMura/attaccoMura_3.jpg";
var bgScontroEsercito = "img/imgGioco/attaccoTruppe/attaccoTruppe_2.jpg";
var bgVittoria = "img/imgGioco/vittoria.jpg";
var bgSconfitta = "img/imgGioco/sconfitta/sconfitta.jpg";

var mostraCittaConquistate = document.getElementById("cittaConquistate");
var cittaConquistate = 0;
var mostraUnitaArmata = document.getElementById("unitaArmata"); 
mostraUnitaArmata.innerHTML = "Unità nella tua armata: " + numeroArmata;
    
// citta verde
    var vitaMuraVerdi = CITTAVERDE.VITAMURA;
    var numeroNemiciVerdi = CITTAVERDE.UNITA;
    
    function assedioVerde(){
        document.getElementById("btnIniziaAttacco").style.display = "none";
        var ripetizioneMuraVerdi = setInterval(attaccaVerdi, 1000);
        mostraQui.style.display = "none";

        function attaccaVerdi(){
            var danno = Math.random();
                danno = danno * ARMATA.DANNO + 1;
                danno = Math.floor(danno);	
            
            var dannoArcieri = Math.random();
                dannoArcieri = dannoArcieri * CITTAVERDE.DANNO + 1; 
                dannoArcieri = Math.floor(dannoArcieri);	

            imgContestuale.src = bgMura;

            vitaMuraVerdi = vitaMuraVerdi - danno;
            document.getElementById("vitaMura").innerHTML = vitaMuraVerdi;

            numeroArmata = numeroArmata - dannoArcieri;
            document.getElementById("numArmata").innerHTML = numeroArmata;	
            mostraQui.style.display = "none";

            var totVitaMura = CITTAVERDE.VITAMURA;
            var percentualeVitaMura = (vitaMuraVerdi / totVitaMura) * 100;
            if(percentualeVitaMura <= 50){
                document.getElementById("schizzo1").style.display = "block";
            }
            if (vitaMuraVerdi <= 0) {
                clearInterval(ripetizioneMuraVerdi);
                document.getElementById("vitaMura").innerHTML = "0";

                mostraQui.style.display = "block";
                mostraQui.innerHTML = messaggioMuroDistrutto;
                imgContestuale.remove();
    
                setTimeout(function(){
                    document.getElementById("divMura").style.display = "none";
                    creaImgContestuale();
                    imgContestuale.src = bgScontroEsercito; 
                    divRapportoForze.style.display = "block";
                    mostraQui.style.display = "none";

                    setTimeout(function(){
                        var ripetizioneAttaccaNemici = setInterval(attaccaAvversariVerdi, 1000);
                        var ripetizioneSubisciAttacco = setInterval(subisciAttaccoVerde, 1000);
                            
                        function attaccaAvversariVerdi(){
                            var dannoArmata = Math.random();
                                dannoArmata = dannoArmata * ARMATA.DANNO + 1;
                                dannoArmata = Math.floor(dannoArmata);	
                        
                            numeroNemiciVerdi = numeroNemiciVerdi - dannoArmata;
                            document.getElementById("numNemici").innerHTML = numeroNemiciVerdi;
                            var totArmataAvversaria = CITTAVERDE.UNITA;
                            var percentualeArmataAvversaria = (numeroNemiciVerdi / totArmataAvversaria) * 100;
                            if(percentualeArmataAvversaria < 90){
                                document.getElementById("schizzo2").style.display = "block";
                            }                            
                            if(percentualeArmataAvversaria < 70){
                                document.getElementById("schizzo3").style.display = "block";
                            }
                            if(percentualeArmataAvversaria < 50){
                                document.getElementById("schizzo4").style.display = "block";
                            }
                            if(percentualeArmataAvversaria < 30){
                                document.getElementById("schizzo5").style.display = "block";
                            }
                            if(percentualeArmataAvversaria < 10){
                                document.getElementById("schizzo6").style.display = "block";
                            }
                            var tot = numeroArmata + numeroNemiciVerdi;
                            var percentualeNemici = (numeroNemiciVerdi / tot) * 100;
                            progressNemici.style.width = percentualeNemici + "%";

                            if(numeroNemiciVerdi <= 0){
                                clearInterval(ripetizioneAttaccaNemici);
                                clearInterval(ripetizioneSubisciAttacco);
                                document.getElementById("numNemici").innerHTML = "0";
                                
                                mostraQui.style.display = "block";
                                mostraQui.innerHTML = messaggioVittoria;
                                
                                progressNemici.style.width = "0px";
                                progressArmata.style.width = "100%";
                                
                                imgContestuale.src = bgVittoria;
                                
                                setTimeout(function(){
                                    cittaConquistate++;
                                    document.getElementById("backgroundAssedio").style.display = "none";
                                    document.getElementById("divAssedio").style.display = "none";
                                    divClanArmata.style.display = "none";
                                    mostraQui.style.display = "none";
                                    imgContestuale.remove();
                                    divRapportoForze.style.display = "none";
                                    
                                    mostraCittaConquistate.innerHTML = "Città in tuo possesso: " + cittaConquistate; 
                                    
                                    var truppeAquisite = ((CITTAVERDE.UNITA + CITTAVERDE.VITAMURA) * 40) / 100;
                                    truppeAquisite = Math.floor(truppeAquisite);
                                    numeroArmata = numeroArmata + truppeAquisite;
                                    mostraUnitaArmata.innerHTML = "Unità nella tua armata: " + numeroArmata;
                                    
                                    document.getElementById("imgCella_" + CITTAVERDE.POSIZIONE).src = pathImg + "clan/clanPiccoli/armata.png";
                                    document.getElementById("btnIniziaAttacco").removeEventListener("click", assedioVerde);
                                    
                                    if(cittaConquistate == 5){
                                        vittoriaGenerale();
                                    }
                                    
                                    ARMATA.DANNO = ARMATA.DANNO + 2;
                                    document.getElementById("btnAttacco").style.display = "none";
                                    
                                    togliSfocatura();
                                    document.getElementById("imgClanNemico").style.display = "none";
                                    removeImgClan();
                                    noSchizzi();
                                }, 5000);
                            }
                        }
                        function subisciAttaccoVerde(){
                            document.getElementById("numArmata").innerHTML = numeroArmata;	
                            var dannoAvversario = Math.random();
                                dannoAvversario = dannoAvversario * CITTAVERDE.DANNO + 1;
                                dannoAvversario = Math.floor(dannoAvversario);
                                
                            numeroArmata = numeroArmata - dannoAvversario;
                            document.getElementById("numArmata").innerHTML = numeroArmata;	
                            
                            var tot = numeroArmata + numeroNemiciVerdi;
                            var percentualeArmata = (numeroArmata / tot) * 100;
                            progressArmata.style.width = percentualeArmata + "%";
                            
                            
                            if(numeroArmata <= 0){
                                clearInterval(ripetizioneAttaccaNemici);
                                clearInterval(ripetizioneSubisciAttacco);
                                document.getElementById("numArmata").innerHTML = "0";
                                
                                mostraQui.style.display = "block";
                                mostraQui.innerHTML = messaggioSconfitta;
                                imgContestuale.src = bgSconfitta;
                                
                                progressArmata.style.width = "0px";
                                progressNemici.style.width = "100%";
                                    
                                setTimeout(function(){ 
                                    mostraCittaConquistate.innerHTML = "Città in tuo possesso: " + cittaConquistate;
                                    imgContestuale.remove();
                                    document.getElementById("btnIniziaAttacco").removeEventListener("click", assedioVerde);
                                    togliSfocatura();
                                    sconfittaGenerale();
                                }, 3000);
                            }
                        }
                    }, 1000);
                }, 4000);
            }
        
            if (numeroArmata <= 0){
                clearInterval(ripetizioneMuraVerdi);
                document.getElementById("numArmata").innerHTML = "0";
                
                mostraQui.style.display = "block";
                mostraQui.innerHTML = messaggioSconfitta;
                imgContestuale.src = bgSconfitta;
                
                progressArmata.style.width = "0px";
                progressNemici.style.width = "100%";
                        
                setTimeout(function(){ 
                    mostraCittaConquistate.innerHTML = "Città in tuo possesso: " + cittaConquistate;
                    imgContestuale.remove();
                    document.getElementById("btnIniziaAttacco").removeEventListener("click", assedioVerde);
                    togliSfocatura();
                    sconfittaGenerale();
                }, 3000);
            }
        }
    }
// citta rossa
    var vitaMuraRosse = CITTAROSSA.VITAMURA;
    var numeroNemiciRossi = CITTAROSSA.UNITA;

    function assedioRosso(){
        document.getElementById("btnIniziaAttacco").style.display = "none";
        var ripetizioneMuraRosse = setInterval(attaccaRossi, 1000);
        mostraQui.style.display = "none";

        function attaccaRossi(){
            var danno = Math.random();
                danno = danno * ARMATA.DANNO + 1;
                danno = Math.floor(danno);	
            
            var dannoArcieri = Math.random();
                dannoArcieri = dannoArcieri * CITTAROSSA.DANNO + 1; 
                dannoArcieri = Math.floor(dannoArcieri);	

            
            imgContestuale.src = bgMura;

            vitaMuraRosse = vitaMuraRosse - danno;
            document.getElementById("vitaMura").innerHTML = vitaMuraRosse;

            numeroArmata = numeroArmata - dannoArcieri;
            document.getElementById("numArmata").innerHTML = numeroArmata;	
            mostraQui.style.display = "none";

            
            var totVitaMura = CITTAROSSA.VITAMURA;
            var percentualeVitaMura = (vitaMuraRosse / totVitaMura) * 100;
            if(percentualeVitaMura <= 50){
                document.getElementById("schizzo1").style.display = "block";
            }
            
            if (vitaMuraRosse <= 0) {
                clearInterval(ripetizioneMuraRosse);
                document.getElementById("vitaMura").innerHTML = "0";

                mostraQui.style.display = "block";
                mostraQui.innerHTML = messaggioMuroDistrutto;

                imgContestuale.remove();

                setTimeout(function(){
                    document.getElementById("divMura").style.display = "none";
                    creaImgContestuale();
                    imgContestuale.src = bgScontroEsercito; 
                    divRapportoForze.style.display = "block";
                    mostraQui.style.display = "none";

                    setTimeout(function(){
                        var ripetizioneAttaccaNemici = setInterval(attaccaAvversariRossi, 1000);
                        var ripetizioneSubisciAttacco = setInterval(subisciAttaccoRossi, 1000);
                            
                        function attaccaAvversariRossi(){
                            var dannoArmata = Math.random();
                                dannoArmata = dannoArmata * ARMATA.DANNO + 1;
                                dannoArmata = Math.floor(dannoArmata);	
                        
                            numeroNemiciRossi = numeroNemiciRossi - dannoArmata;
                            document.getElementById("numNemici").innerHTML = numeroNemiciRossi;
                            
                            var totArmataAvversaria = CITTAROSSA.UNITA;
                            var percentualeArmataAvversaria = (numeroNemiciRossi / totArmataAvversaria) * 100;
                            if(percentualeArmataAvversaria < 90){
                                document.getElementById("schizzo2").style.display = "block";
                            }                            
                            if(percentualeArmataAvversaria < 70){
                                document.getElementById("schizzo3").style.display = "block";
                            }
                            if(percentualeArmataAvversaria < 50){
                                document.getElementById("schizzo4").style.display = "block";
                            }
                            if(percentualeArmataAvversaria < 30){
                                document.getElementById("schizzo5").style.display = "block";
                            }
                            if(percentualeArmataAvversaria < 10){
                                document.getElementById("schizzo6").style.display = "block";
                            }
                            
                            var tot = numeroArmata + numeroNemiciRossi;
                            var percentualeNemici = (numeroNemiciRossi / tot) * 100;
                            progressNemici.style.width = percentualeNemici + "%";

                            
                            if(numeroNemiciRossi <= 0){
                                clearInterval(ripetizioneAttaccaNemici);
                                clearInterval(ripetizioneSubisciAttacco);
                                document.getElementById("numNemici").innerHTML = "0";
                                
                                mostraQui.style.display = "block";
                                mostraQui.innerHTML = messaggioVittoria;
                                
                                progressNemici.style.width = "0px";
                                progressArmata.style.width = "100%";
                                
                                imgContestuale.src = bgVittoria;
                                
                                setTimeout(function(){
                                    cittaConquistate++;
                                    document.getElementById("backgroundAssedio").style.display = "none";
                                    document.getElementById("divAssedio").style.display = "none";
                                    divClanArmata.style.display = "none";
                                    mostraQui.style.display = "none";
                                    imgContestuale.remove();
                                    divRapportoForze.style.display = "none";
                                    mostraCittaConquistate.innerHTML = "Città in tuo possesso: " + cittaConquistate; 
                                    var truppeAquisite = ((CITTAROSSA.UNITA + CITTAROSSA.VITAMURA) * 40) / 100;
                                    truppeAquisite = Math.floor(truppeAquisite);
                                    numeroArmata = numeroArmata + truppeAquisite;
                                    mostraUnitaArmata.innerHTML = "Unità nella tua armata: " + numeroArmata;
                                    document.getElementById("imgCella_" + CITTAROSSA.POSIZIONE).src = pathImg + "clan/clanPiccoli/armata.png";
                                    document.getElementById("btnIniziaAttacco").removeEventListener("click", assedioRosso);
                                    togliSfocatura();
                                    noSchizzi();
                                    removeImgClan();
                                    if(cittaConquistate == 5){
                                        vittoriaGenerale();
                                    }
                                    ARMATA.DANNO = ARMATA.DANNO + 2;
                                    document.getElementById("btnAttacco").style.display = "none";
                                }, 5000);
                            }
                        }
                        function subisciAttaccoRossi(){
                            document.getElementById("numArmata").innerHTML = numeroArmata;	
                            var dannoAvversario = Math.random();
                                dannoAvversario = dannoAvversario * CITTAROSSA.DANNO + 1;
                                dannoAvversario = Math.floor(dannoAvversario);
                                
                            numeroArmata = numeroArmata - dannoAvversario;
                            document.getElementById("numArmata").innerHTML = numeroArmata;	
                            
                            var tot = numeroArmata + numeroNemiciRossi;
                            var percentualeArmata = (numeroArmata / tot) * 100;
                            progressArmata.style.width = percentualeArmata + "%";
                            
                            
                            if(numeroArmata <= 0){
                                clearInterval(ripetizioneAttaccaNemici);
                                clearInterval(ripetizioneSubisciAttacco);
                                document.getElementById("numArmata").innerHTML = "0";
                                
                                mostraQui.style.display = "block";
                                mostraQui.innerHTML = messaggioSconfitta;
                                imgContestuale.src = bgSconfitta;
                                
                                progressArmata.style.width = "0px";
                                progressNemici.style.width = "100%";
                                    
                                setTimeout(function(){ 
                                    mostraCittaConquistate.innerHTML = "Città in tuo possesso: " + cittaConquistate;
                                    imgContestuale.remove();
                                    document.getElementById("btnIniziaAttacco").removeEventListener("click", assedioRosso);
                                    togliSfocatura();
                                    sconfittaGenerale();
                                }, 3000);
                            }
                        }
                    }, 1000);
                }, 4000);
            }
            
            if (numeroArmata <= 0){
                clearInterval(ripetizioneMuraRosse);
                document.getElementById("numArmata").innerHTML = "0";
                
                mostraQui.style.display = "block";
                mostraQui.innerHTML = messaggioSconfitta;
                imgContestuale.src = bgSconfitta;
                
                progressArmata.style.width = "0px";
                progressNemici.style.width = "100%";
                        
                setTimeout(function(){ 
                    mostraCittaConquistate.innerHTML = "Città in tuo possesso: " + cittaConquistate;
                    imgContestuale.remove();
                    document.getElementById("btnIniziaAttacco").removeEventListener("click", assedioRosso);
                    togliSfocatura();
                    sconfittaGenerale();
                }, 3000);
            }
        }
    }
// citta marrone
    var vitaMuraMarroni = CITTAMARRONE.VITAMURA;
    var numeroNemiciMarroni = CITTAMARRONE.UNITA;

    function assedioMarrone(){
        document.getElementById("btnIniziaAttacco").style.display = "none";
        var ripetizioneMuraMarroni = setInterval(attaccaMarroni, 1000);
        mostraQui.style.display = "none";

        function attaccaMarroni(){
            var danno = Math.random();
                danno = danno * ARMATA.DANNO + 1;
                danno = Math.floor(danno);	
            
            var dannoArcieri = Math.random();
                dannoArcieri = dannoArcieri * CITTAMARRONE.DANNO + 1;
                dannoArcieri = Math.floor(dannoArcieri);	

            
            imgContestuale.src = bgMura;

            vitaMuraMarroni = vitaMuraMarroni - danno;
            document.getElementById("vitaMura").innerHTML = vitaMuraMarroni;

            numeroArmata = numeroArmata - dannoArcieri;
            document.getElementById("numArmata").innerHTML = numeroArmata;	
            mostraQui.style.display = "none";

            var totVitaMura = CITTAMARRONE.VITAMURA;
            var percentualeVitaMura = (vitaMuraMarroni / totVitaMura) * 100;
            if(percentualeVitaMura <= 50){
                document.getElementById("schizzo1").style.display = "block";
            }
            
            if (vitaMuraMarroni <= 0) {
                clearInterval(ripetizioneMuraMarroni);
                document.getElementById("vitaMura").innerHTML = "0";

                mostraQui.style.display = "block";
                mostraQui.innerHTML = messaggioMuroDistrutto;
                imgContestuale.remove();

                setTimeout(function(){
                    document.getElementById("divMura").style.display = "none";
                    
                    creaImgContestuale();
                    imgContestuale.src = bgScontroEsercito; 
                    divRapportoForze.style.display = "block";
                    mostraQui.style.display = "none";

                    setTimeout(function(){
                        var ripetizioneAttaccaNemici = setInterval(attaccaAvversariMarroni, 1000);
                        var ripetizioneSubisciAttacco = setInterval(subisciAttaccoMarroni, 1000);
                            
                        function attaccaAvversariMarroni(){
                            var dannoArmata = Math.random();
                                dannoArmata = dannoArmata * ARMATA.DANNO + 1;
                                dannoArmata = Math.floor(dannoArmata);	
                        
                            numeroNemiciMarroni = numeroNemiciMarroni - dannoArmata;
                            document.getElementById("numNemici").innerHTML = numeroNemiciMarroni;
                            
                            var totArmataAvversaria = CITTAMARRONE.UNITA;
                            var percentualeArmataAvversaria = (numeroNemiciMarroni / totArmataAvversaria) * 100;
                            if(percentualeArmataAvversaria < 90){
                                document.getElementById("schizzo2").style.display = "block";
                            }                            
                            if(percentualeArmataAvversaria < 70){
                                document.getElementById("schizzo3").style.display = "block";
                            }
                            if(percentualeArmataAvversaria < 50){
                                document.getElementById("schizzo4").style.display = "block";
                            }
                            if(percentualeArmataAvversaria < 30){
                                document.getElementById("schizzo5").style.display = "block";
                            }
                            if(percentualeArmataAvversaria < 10){
                                document.getElementById("schizzo6").style.display = "block";
                            }
                            
                            var tot = numeroArmata + numeroNemiciMarroni;
                            var percentualeNemici = (numeroNemiciMarroni / tot) * 100;
                            progressNemici.style.width = percentualeNemici + "%";

                            
                            if(numeroNemiciMarroni <= 0){
                                clearInterval(ripetizioneAttaccaNemici);
                                clearInterval(ripetizioneSubisciAttacco);
                                document.getElementById("numNemici").innerHTML = "0";
                                
                                mostraQui.style.display = "block";
                                mostraQui.innerHTML = messaggioVittoria;
                                
                                progressNemici.style.width = "0px";
                                progressArmata.style.width = "100%";
                                
                                imgContestuale.src = bgVittoria;
                                
                                setTimeout(function(){
                                    cittaConquistate++;
                                    document.getElementById("backgroundAssedio").style.display = "none";
                                    document.getElementById("divAssedio").style.display = "none";
                                    divClanArmata.style.display = "none";
                                    mostraQui.style.display = "none";
                                    imgContestuale.remove();
                                    divRapportoForze.style.display = "none";
                                    
                                    mostraCittaConquistate.innerHTML = "Città in tuo possesso: " + cittaConquistate; 
                                    
                                    var truppeAquisite = ((CITTAMARRONE.UNITA + CITTAMARRONE.VITAMURA) * 40) / 100;
                                    truppeAquisite = Math.floor(truppeAquisite);
                                    numeroArmata = numeroArmata + truppeAquisite;
                                    mostraUnitaArmata.innerHTML = "Unità nella tua armata: " + numeroArmata;
                                    
                                    document.getElementById("imgCella_" + CITTAMARRONE.POSIZIONE).src = pathImg + "clan/clanPiccoli/armata.png";
                                    document.getElementById("btnIniziaAttacco").removeEventListener("click", assedioMarrone);
                                    
                                    togliSfocatura();
                                    if(cittaConquistate == 5){
                                        vittoriaGenerale();
                                    }
                                    noSchizzi();
                                    removeImgClan();
                                    
                                    ARMATA.DANNO = ARMATA.DANNO + 2;
                                    document.getElementById("btnAttacco").style.display = "none";
                                }, 5000);
                            }
                        }
                        function subisciAttaccoMarroni(){
                            document.getElementById("numArmata").innerHTML = numeroArmata;	
                            var dannoAvversario = Math.random();
                                dannoAvversario = dannoAvversario * CITTAMARRONE.DANNO + 1;
                                dannoAvversario = Math.floor(dannoAvversario);
                                
                            numeroArmata = numeroArmata - dannoAvversario;
                            document.getElementById("numArmata").innerHTML = numeroArmata;	
                            
                            var tot = numeroArmata + numeroNemiciMarroni;
                            var percentualeArmata = (numeroArmata / tot) * 100;
                            progressArmata.style.width = percentualeArmata + "%";
                            
                            
                            if(numeroArmata <= 0){
                                clearInterval(ripetizioneAttaccaNemici);
                                clearInterval(ripetizioneSubisciAttacco);
                                document.getElementById("numArmata").innerHTML = "0";
                                
                                mostraQui.style.display = "block";
                                mostraQui.innerHTML = messaggioSconfitta;
                                imgContestuale.src = bgSconfitta;
                                
                                progressArmata.style.width = "0px";
                                progressNemici.style.width = "100%";
                                    
                                setTimeout(function(){ 
                                    mostraCittaConquistate.innerHTML = "Città in tuo possesso: " + cittaConquistate;
                                    imgContestuale.remove();
                                    document.getElementById("btnIniziaAttacco").removeEventListener("click", assedioMarrone);
                                    togliSfocatura();
                                    sconfittaGenerale();
                                }, 3000);
                            }
                        }
                    }, 1000);
                }, 4000);
            }
            
            if (numeroArmata <= 0){
                clearInterval(ripetizioneMuraMarroni);
                document.getElementById("numArmata").innerHTML = "0";
                
                mostraQui.style.display = "block";
                mostraQui.innerHTML = messaggioSconfitta;
                imgContestuale.src = bgSconfitta;
                
                progressArmata.style.width = "0px";
                progressNemici.style.width = "100%";
                        
                setTimeout(function(){ 
                    mostraCittaConquistate.innerHTML = "Città in tuo possesso: " + cittaConquistate;
                    imgContestuale.remove();
                    document.getElementById("btnIniziaAttacco").removeEventListener("click", assedioMarrone);
                    togliSfocatura();
                    sconfittaGenerale();
                }, 3000);
            }
        }
    }
// citta viola
    var vitaMuraViola = CITTAVIOLA.VITAMURA;
    var numeroNemiciViola = CITTAVIOLA.UNITA;

    function assedioViola(){
        document.getElementById("btnIniziaAttacco").style.display = "none";
        var ripetizioneMuraViola = setInterval(attaccaViola, 1000);
        mostraQui.style.display = "none";

        function attaccaViola(){
            var danno = Math.random();
                danno = danno * ARMATA.DANNO + 1;
                danno = Math.floor(danno);	
            
            var dannoArcieri = Math.random();
                dannoArcieri = dannoArcieri * CITTAVIOLA.DANNO + 1;
                dannoArcieri = Math.floor(dannoArcieri);	

            
            imgContestuale.src = bgMura;

            vitaMuraViola = vitaMuraViola - danno;
            document.getElementById("vitaMura").innerHTML = vitaMuraViola;

            numeroArmata = numeroArmata - dannoArcieri;
            document.getElementById("numArmata").innerHTML = numeroArmata;	
            mostraQui.style.display = "none";

            var totVitaMura = CITTAVIOLA.VITAMURA;
            var percentualeVitaMura = (vitaMuraViola / totVitaMura) * 100;
            if(percentualeVitaMura <= 50){
                document.getElementById("schizzo1").style.display = "block";
            }
            
            if (vitaMuraViola <= 0) {
                clearInterval(ripetizioneMuraViola);
                document.getElementById("vitaMura").innerHTML = "0";

                mostraQui.style.display = "block";
                mostraQui.innerHTML = messaggioMuroDistrutto;
                imgContestuale.remove();

                setTimeout(function(){
                    document.getElementById("divMura").style.display = "none";
                    
                    creaImgContestuale();
                    imgContestuale.src = bgScontroEsercito; 
                    divRapportoForze.style.display = "block";
                    mostraQui.style.display = "none";

                    setTimeout(function(){
                        var ripetizioneAttaccaNemici = setInterval(attaccaAvversariViola, 1000);
                        var ripetizioneSubisciAttacco = setInterval(subisciAttaccoViola, 1000);
                            
                        function attaccaAvversariViola(){
                            var dannoArmata = Math.random();
                                dannoArmata = dannoArmata * ARMATA.DANNO + 1;
                                dannoArmata = Math.floor(dannoArmata);	
                        
                            numeroNemiciViola = numeroNemiciViola - dannoArmata;
                            document.getElementById("numNemici").innerHTML = numeroNemiciViola;
                            
                            var totArmataAvversaria = CITTAVIOLA.UNITA;
                            var percentualeArmataAvversaria = (numeroNemiciViola / totArmataAvversaria) * 100;
                            if(percentualeArmataAvversaria < 90){
                                document.getElementById("schizzo2").style.display = "block";
                            }                            
                            if(percentualeArmataAvversaria < 70){
                                document.getElementById("schizzo3").style.display = "block";
                            }
                            if(percentualeArmataAvversaria < 50){
                                document.getElementById("schizzo4").style.display = "block";
                            }
                            if(percentualeArmataAvversaria < 30){
                                document.getElementById("schizzo5").style.display = "block";
                            }
                            if(percentualeArmataAvversaria < 10){
                                document.getElementById("schizzo6").style.display = "block";
                            }
                            
                            var tot = numeroArmata + numeroNemiciViola;
                            var percentualeNemici = (numeroNemiciViola / tot) * 100;
                            progressNemici.style.width = percentualeNemici + "%";

                            
                            if(numeroNemiciViola <= 0){
                                clearInterval(ripetizioneAttaccaNemici);
                                clearInterval(ripetizioneSubisciAttacco);
                                document.getElementById("numNemici").innerHTML = "0";
                                
                                mostraQui.style.display = "block";
                                mostraQui.innerHTML = messaggioVittoria;
                                
                                progressNemici.style.width = "0px";
                                progressArmata.style.width = "100%";
                                
                                imgContestuale.src = bgVittoria;
                                
                                setTimeout(function(){
                                    cittaConquistate++;
                                    document.getElementById("backgroundAssedio").style.display = "none";
                                    document.getElementById("divAssedio").style.display = "none";
                                    divClanArmata.style.display = "none";
                                    mostraQui.style.display = "none";
                                    imgContestuale.remove();
                                    divRapportoForze.style.display = "none";
                                    
                                    mostraCittaConquistate.innerHTML = "Città in tuo possesso: " + cittaConquistate; 
                                    
                                    var truppeAquisite = ((CITTAVIOLA.UNITA + CITTAVIOLA.VITAMURA) * 40) / 100;
                                    truppeAquisite = Math.floor(truppeAquisite);
                                    numeroArmata = numeroArmata + truppeAquisite;
                                    mostraUnitaArmata.innerHTML = "Unità nella tua armata: " + numeroArmata;
                                    
                                    document.getElementById("imgCella_" + CITTAVIOLA.POSIZIONE).src = pathImg + "clan/clanPiccoli/armata.png";
                                    document.getElementById("btnIniziaAttacco").removeEventListener("click", assedioViola);
                                    
                                    togliSfocatura();
                                     generale
                                    if(cittaConquistate == 5){
                                        vittoriaGenerale();
                                    }
                                    noSchizzi();
                                    removeImgClan();
                                    
                                    ARMATA.DANNO = ARMATA.DANNO + 2;
                                    document.getElementById("btnAttacco").style.display = "none";
                                }, 5000);
                            }
                        }
                        function subisciAttaccoViola(){
                            document.getElementById("numArmata").innerHTML = numeroArmata;	
                            var dannoAvversario = Math.random();
                                dannoAvversario = dannoAvversario * CITTAVIOLA.DANNO + 1;
                                dannoAvversario = Math.floor(dannoAvversario);
                                
                            numeroArmata = numeroArmata - dannoAvversario;
                            document.getElementById("numArmata").innerHTML = numeroArmata;	
                            
                            var tot = numeroArmata + numeroNemiciViola;
                            var percentualeArmata = (numeroArmata / tot) * 100;
                            progressArmata.style.width = percentualeArmata + "%";
                            
                            
                            if(numeroArmata <= 0){
                                clearInterval(ripetizioneAttaccaNemici);
                                clearInterval(ripetizioneSubisciAttacco);
                                document.getElementById("numArmata").innerHTML = "0";
                                
                                mostraQui.style.display = "block";
                                mostraQui.innerHTML = messaggioSconfitta;
                                imgContestuale.src = bgSconfitta;
                                
                                progressArmata.style.width = "0px";
                                progressNemici.style.width = "100%";
                                    
                                setTimeout(function(){ 
                                    mostraCittaConquistate.innerHTML = "Città in tuo possesso: " + cittaConquistate;
                                    imgContestuale.remove();
                                    document.getElementById("btnIniziaAttacco").removeEventListener("click", assedioViola);
                                    togliSfocatura();
                                    sconfittaGenerale();
                                }, 3000);
                            }
                        }
                    }, 1000);
                }, 4000);
            }
            
            if (numeroArmata <= 0){
                clearInterval(ripetizioneMuraViola);
                document.getElementById("numArmata").innerHTML = "0";
                
                mostraQui.style.display = "block";
                mostraQui.innerHTML = messaggioSconfitta;
                creaImgContestuale();
                imgContestuale.src = bgSconfitta;
                
                progressArmata.style.width = "0px";
                progressNemici.style.width = "100%";
                        
                setTimeout(function(){ 
                    mostraCittaConquistate.innerHTML = "Città in tuo possesso: " + cittaConquistate;
                    imgContestuale.remove();
                    document.getElementById("btnIniziaAttacco").removeEventListener("click", assedioVerde);
                    togliSfocatura();
                    sconfittaGenerale();
                }, 3000);
            }
        }
    }
// citta blu
    var vitaMuraBlu = CITTABLU.VITAMURA;
    var numeroNemiciBlu = CITTABLU.UNITA;

    function assedioBlu(){
        document.getElementById("btnIniziaAttacco").style.display = "none";
        var ripetizioneMuraBlu = setInterval(attaccaBlu, 1000);
        mostraQui.style.display = "none";

        function attaccaBlu(){
            var danno = Math.random();
                danno = danno * ARMATA.DANNO + 1;
                danno = Math.floor(danno);	
            
            var dannoArcieri = Math.random();
                dannoArcieri = dannoArcieri * CITTABLU.DANNO + 1;
                dannoArcieri = Math.floor(dannoArcieri);	

            
            imgContestuale.src = bgMura;

            vitaMuraBlu = vitaMuraBlu - danno;
            document.getElementById("vitaMura").innerHTML = vitaMuraBlu;

            numeroArmata = numeroArmata - dannoArcieri;
            document.getElementById("numArmata").innerHTML = numeroArmata;	
            mostraQui.style.display = "none";

            var totVitaMura = CITTABLU.VITAMURA;
            var percentualeVitaMura = (vitaMuraBlu / totVitaMura) * 100;
            if(percentualeVitaMura <= 50){
                document.getElementById("schizzo1").style.display = "block";
            }
            
            if (vitaMuraBlu <= 0) {
                clearInterval(ripetizioneMuraBlu);
                document.getElementById("vitaMura").innerHTML = "0";

                mostraQui.style.display = "block";
                mostraQui.innerHTML = messaggioMuroDistrutto;
                imgContestuale.remove();

                setTimeout(function(){
                    document.getElementById("divMura").style.display = "none";
                    creaImgContestuale();
                    imgContestuale.src = bgScontroEsercito; 
                    divRapportoForze.style.display = "block";
                    mostraQui.style.display = "none";

                    setTimeout(function(){
                        var ripetizioneAttaccaNemici = setInterval(attaccaAvversariBlu, 1000);
                        var ripetizioneSubisciAttacco = setInterval(subisciAttaccoBlu, 1000);
                            
                        function attaccaAvversariBlu(){
                            var dannoArmata = Math.random();
                                dannoArmata = dannoArmata * ARMATA.DANNO + 1;
                                dannoArmata = Math.floor(dannoArmata);	
                        
                            numeroNemiciBlu = numeroNemiciBlu - dannoArmata;
                            document.getElementById("numNemici").innerHTML = numeroNemiciBlu;
                            
                            var totArmataAvversaria = CITTABLU.UNITA;
                            var percentualeArmataAvversaria = (numeroNemiciBlu / totArmataAvversaria) * 100;
                            if(percentualeArmataAvversaria < 90){
                                document.getElementById("schizzo2").style.display = "block";
                            }                            
                            if(percentualeArmataAvversaria < 70){
                                document.getElementById("schizzo3").style.display = "block";
                            }
                            if(percentualeArmataAvversaria < 50){
                                document.getElementById("schizzo4").style.display = "block";
                            }
                            if(percentualeArmataAvversaria < 30){
                                document.getElementById("schizzo5").style.display = "block";
                            }
                            if(percentualeArmataAvversaria < 10){
                                document.getElementById("schizzo6").style.display = "block";
                            }
                            
                            var tot = numeroArmata + numeroNemiciBlu;
                            var percentualeNemici = (numeroNemiciBlu / tot) * 100;
                            progressNemici.style.width = percentualeNemici + "%";

                            
                            if(numeroNemiciBlu <= 0){
                                clearInterval(ripetizioneAttaccaNemici);
                                clearInterval(ripetizioneSubisciAttacco);
                                document.getElementById("numNemici").innerHTML = "0";
                                
                                mostraQui.style.display = "block";
                                mostraQui.innerHTML = messaggioVittoria;
                                
                                progressNemici.style.width = "0px";
                                progressArmata.style.width = "100%";
                                
                                imgContestuale.src = bgVittoria;
                                
                                setTimeout(function(){
                                    cittaConquistate++;
                                    document.getElementById("backgroundAssedio").style.display = "none";
                                    document.getElementById("divAssedio").style.display = "none";
                                    divClanArmata.style.display = "none";
                                    mostraQui.style.display = "none";
                                    imgContestuale.remove();
                                    divRapportoForze.style.display = "none";
                                    
                                    mostraCittaConquistate.innerHTML = "Città in tuo possesso: " + cittaConquistate; 
                                    
                                    var truppeAquisite = ((CITTABLU.UNITA + CITTABLU.VITAMURA) * 40) / 100;
                                    truppeAquisite = Math.floor(truppeAquisite);
                                    numeroArmata = numeroArmata + truppeAquisite;
                                    mostraUnitaArmata.innerHTML = "Unità nella tua armata: " + numeroArmata;
                                    
                                    document.getElementById("imgCella_" + CITTABLU.POSIZIONE).src = pathImg + "clan/clanPiccoli/armata.png";
                                    document.getElementById("btnIniziaAttacco").removeEventListener("click", assedioBlu);
                                    
                                    togliSfocatura();
                                     generale
                                    if(cittaConquistate == 5){
                                        vittoriaGenerale();
                                    }
                                    noSchizzi();
                                    removeImgClan();
                                    
                                    ARMATA.DANNO = ARMATA.DANNO + 2;
                                    document.getElementById("btnAttacco").style.display = "none";
                                }, 5000);
                            }
                        }
                        function subisciAttaccoBlu(){
                            document.getElementById("numArmata").innerHTML = numeroArmata;	
                            var dannoAvversario = Math.random();
                                dannoAvversario = dannoAvversario * CITTABLU.DANNO + 1;
                                dannoAvversario = Math.floor(dannoAvversario);
                                
                            numeroArmata = numeroArmata - dannoAvversario;
                            document.getElementById("numArmata").innerHTML = numeroArmata;	
                            
                            var tot = numeroArmata + numeroNemiciBlu;
                            var percentualeArmata = (numeroArmata / tot) * 100;
                            progressArmata.style.width = percentualeArmata + "%";
                            
                            
                            if(numeroArmata <= 0){
                                clearInterval(ripetizioneAttaccaNemici);
                                clearInterval(ripetizioneSubisciAttacco);
                                document.getElementById("numArmata").innerHTML = "0";
                                
                                mostraQui.style.display = "block";
                                mostraQui.innerHTML = messaggioSconfitta;
                                imgContestuale.src = bgSconfitta;
                                
                                progressArmata.style.width = "0px";
                                progressNemici.style.width = "100%";
                                    
                                setTimeout(function(){ 
                                    mostraCittaConquistate.innerHTML = "Città in tuo possesso: " + cittaConquistate;
                                    imgContestuale.remove();
                                    document.getElementById("btnIniziaAttacco").removeEventListener("click", assedioVerde);
                                    togliSfocatura();
                                    sconfittaGenerale();
                                }, 3000);
                            }
                        }
                    }, 1000);
                }, 4000);
            }
            
            if (numeroArmata <= 0){
                clearInterval(ripetizioneMuraBlu);
                document.getElementById("numArmata").innerHTML = "0";
                
                mostraQui.style.display = "block";
                mostraQui.innerHTML = messaggioSconfitta;
                imgContestuale.src = bgSconfitta;
                
                progressArmata.style.width = "0px";
                progressNemici.style.width = "100%";
                        
                setTimeout(function(){ 
                    mostraCittaConquistate.innerHTML = "Città in tuo possesso: " + cittaConquistate;
                    imgContestuale.remove();
                    document.getElementById("btnIniziaAttacco").removeEventListener("click", assedioVerde);
                    togliSfocatura();
                    sconfittaGenerale();
                }, 3000);
            }
        }
    }
