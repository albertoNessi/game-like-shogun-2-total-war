// creo un cookie per archiviare il nome del giocatore
  var date;
  function setCookie(cname, cvalue, scadenza) {
    date = new Date();
    date.setTime(date.getTime() + (scadenza*60*1000));
    var scandezaCookie = "scadenza=" + date.toGMTString();
    document.cookie = cname + "=" + cvalue + ";" + scandezaCookie + ";";
  }
// prelevare un cookie precedentemente creato
  var nome;
  function getCookie(cname) {
    nome = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i < ca.length; i++) {
      var c = ca[i];
      while (c.charAt(0) == ' ') {
        c = c.substring(1);
      }
      if (c.indexOf(nome) == 0) {
        return c.substring(nome.length, c.length);
      }
    }
    return "";
  }
// controllare un cookie  
  var user;
  function checkCookie() {
    user = getCookie("nome");
    if (user != "") {
      document.getElementById("divBenvenuto").style.display = "block";
	    document.getElementById("backgroundAssedio").style.display = "block";
      document.getElementById("titoloDivBenvenuto").innerHTML = "Bentornato " + user + "!";
      // inserire i valori reali del detentore del record
      document.getElementById("paragrafoDivBenvenuto").innerHTML = "Il precedente record è detenuto da Alberto, il quale ha vinto con 22 unità rimaste con un tempo di 5 minuti.<br><br>Buona fortuna!"
      sfoca();
    } else {
        sfoca();
        user = prompt("Inserisci il tuo nome:","");
        if (user != "" && user != null) {
          setCookie("nome", user, 30);
          togliSfocatura();
        }
    }
  }