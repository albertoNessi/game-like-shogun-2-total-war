function creaImgContestuale(){
    var imgContestuale = document.createElement("img");
    imgContestuale.id = "imgContestuale";
    imgContestuale.src = "img/imgGioco/castelli/castello_1.jpg";
    var appendiQui = document.getElementById("divImgContestuale");
    appendiQui.appendChild(imgContestuale);
}
function creoImgClan(){
    var imgClanArmata = document.createElement("img");
    imgClanArmata.id = "imgClanArmata";
    imgClanArmata.src = "img/imgGioco/clan/clanArmata.png";
    imgClanArmata.classList.add("imgClan");
    var appendiQui = document.getElementById("divClanArmata");
    appendiQui.appendChild(imgClanArmata); 

    var imgClanArmataNemico = document.createElement("img");
    imgClanArmataNemico.id = "imgClanNemico";
    imgClanArmataNemico.src = "img/imgGioco/clan/clanVerde.png";
    imgClanArmataNemico.classList.add("imgClan");
    var appendiQui2 = document.getElementById("divClanNemico");
    appendiQui2.appendChild(imgClanArmataNemico); 
}
function removeImgClan(){
    var imgClanArmata1 = document.getElementById("imgClanArmata");
    imgClanArmata1.remove();
    var imgClanArmataNemico1 = document.getElementById("imgClanNemico");
    imgClanArmataNemico1.remove();
}
function creaImgSpiegazione(){
    var imgSpiegazione = document.createElement("img");
    imgSpiegazione.id = "imgSpiegazione";
    imgSpiegazione.src = "img/imgGioco/spiegazione.png";
    
    var appendiQui = document.getElementById("divImgSpiegazione");
    appendiQui.appendChild(imgSpiegazione);
}
function removeImgSpiegazione(){
    document.getElementById("imgSpiegazione").remove();
}
function creaImgSchizzi(){
    var imgSchizzo1 = document.createElement("img");
    var imgSchizzo2 = document.createElement("img");
    var imgSchizzo3 = document.createElement("img");
    var imgSchizzo4 = document.createElement("img");
    var imgSchizzo5 = document.createElement("img");
    var imgSchizzo6 = document.createElement("img");
    
    imgSchizzo1.id = "schizzo1";
    imgSchizzo2.id = "schizzo2";
    imgSchizzo3.id = "schizzo3";
    imgSchizzo4.id = "schizzo4";
    imgSchizzo5.id = "schizzo5";
    imgSchizzo6.id = "schizzo6";
    
    imgSchizzo1.classList.add("imgSchizzi");
    imgSchizzo2.classList.add("imgSchizzi");
    imgSchizzo3.classList.add("imgSchizzi");
    imgSchizzo4.classList.add("imgSchizzi");
    imgSchizzo5.classList.add("imgSchizzi");
    imgSchizzo6.classList.add("imgSchizzi");
    
    imgSchizzo1.src = "img/imgGioco/schizzi/schizzo_1.png";
    imgSchizzo2.src = "img/imgGioco/schizzi/schizzo_2.png";
    imgSchizzo3.src = "img/imgGioco/schizzi/schizzo_3.png";
    imgSchizzo4.src = "img/imgGioco/schizzi/schizzo_1.png";
    imgSchizzo5.src = "img/imgGioco/schizzi/schizzo_2.png";
    imgSchizzo6.src = "img/imgGioco/schizzi/schizzo_3.png";
    
    var appendiQui = document.getElementById("divAssedio");
    appendiQui.appendChild(imgSchizzo1);
    appendiQui.appendChild(imgSchizzo2);
    appendiQui.appendChild(imgSchizzo3);
    appendiQui.appendChild(imgSchizzo4);
    appendiQui.appendChild(imgSchizzo5);
    appendiQui.appendChild(imgSchizzo6);
}
function creaImgSconfitta(){
    var imgSconfitta = document.createElement("img");
    imgSconfitta.id = "imgSconfittaGenerale";
    imgSconfitta.src = "img/imgGioco/sconfitta/sconfitta.jpg";
    imgSconfitta.classList.add("imgRisultati");

    var appendiQui3 = document.getElementById("divRisultati");
    appendiQui3.appendChild(imgSconfitta);
}
function creaImgVittoria(){
    var imgSconfitta = document.createElement("img");
    imgSconfitta.id = "imgVittoriaGenerale";
    imgSconfitta.src = "img/imgGioco/vittoria.jpg";
    imgSconfitta.classList.add("imgRisultati");

    var appendiQui3 = document.getElementById("divRisultati");
    appendiQui3.appendChild(imgSconfitta);
}