var disegna = 3; 
var BUCO = 2;  // Costante
var SPINE = 3; // Costante
var FUNGHETTI = 4; // Costante
var percorso = new Array(30);
var COPPA = 5;

// COLORI CELLE
var incrementoColore = 10;
var red = [255, 1, 61];
var lightblue = [10, 216, 253];

var differenza1 = (red[0] - lightblue[0]) / 30;  // 8
var differenza2 = (lightblue[1] - red[1]) / 30;  // 7
var differenza3 = (lightblue[2] - red[2]) / 30;	 // 6	

for (var i=0; i<percorso.length; i++){
    percorso[i]= 0; 
}
			
percorso[3] = BUCO; 
percorso[7] = FUNGHETTI; 
percorso[9] = SPINE; 
percorso[13] = BUCO;
percorso[16] = FUNGHETTI;
percorso[23] = SPINE;
percorso[26] = BUCO; 
percorso[30] = COPPA; 
				
function play(){
	messaggio.innerHTML = "Ora lancia il dado per muoverti nelle caselle";
	var contenuto = document.getElementById("percorso").innerHTML; 
	var btnTabella = document.getElementById("btnCreaTabella");
	var btnLanciaDado = document.getElementById("btnDado");
	var imgDado = document.getElementById("dado");

	contenuto.innerHTML = "PROVA";
	if (contenuto==""){
		contenuto = "Percorso creato: " + percorso;
		// disabilitare il pulsante "crea tabella" dopo averlo premuto
		btnTabella.setAttribute("disabled", "true");
		btnTabella.classList.remove("btnAttivo");
		btnTabella.classList.add("btnDisabled");

		// abilitare l'altro
		imgDado.style.display = "inline-block";
		btnLanciaDado.removeAttribute("disabled");
		btnLanciaDado.classList.remove("btnDisabled");
		btnLanciaDado.classList.add("btnAttivo");
	}
	else {
	    contenuto=""; 
	}
	    generaTabella();
}
// ---------
// PERSONAGGI
// ---------
var uomo = document.getElementById("imgUomo");
var donna = document.getElementById("imgDonna");

// uomo
function uomoIn(){
	uomo.style.filter = "grayscale(0%)";
	// uomo.style.boxShadow = "inset(inset 0px 0px 20px red)";

	uomo.src = "img/imgDado/personaggi/uomo_piccolo_glitch.gif";
	uomo.style.cursor = "pointer";
	setTimeout(function (){
		uomo.src = "img/imgDado/personaggi/uomo_piccolo.png";
	}, 0500);
}
function uomoOut(){
	uomo.style.filter = "grayscale(100%)";
	uomo.src = "img/imgDado/personaggi/uomo_piccolo.png";
}

// donna
function donnaIn(){
	donna.style.filter = "grayscale(0%)";
	donna.src = "img/imgDado/personaggi/donna_piccola_glitch.gif";
	donna.style.cursor = "pointer";
	setTimeout(function (){
		donna.src = "img/imgDado/personaggi/donna_piccolo.png";
	}, 0500);
}
function donnaOut(){
	donna.style.filter = "grayscale(100%)";
	donna.src = "img/imgDado/personaggi/donna_piccolo.png";
}

// scelta personaggio
var posizione = 1; 
var personaggioSelezionato;
var imgPersonaggio; 
var divPersonaggi = document.getElementById("divPersonaggi");

function selezionaUomo(){
	uomo.style.filter = "grayscale(0%)";
	personaggioSelezionato = "uomo";
	document.getElementById("messaggio").innerHTML = "Hai selezionato il ragazzo";
	setTimeout(function (){
		divPersonaggi.style.display = "none";
		document.getElementById("divPulsanti").style.display = "flex";
		document.getElementById("messaggio").innerHTML = "Ora crea la tabella";
	}, 1000);
}
function selezionaDonna(){
	donna.style.filter = "grayscale(0%)";
	personaggioSelezionato = "donna";
	document.getElementById("messaggio").innerHTML = "Hai selezionato la ragazza";
	setTimeout(function (){
		divPersonaggi.style.display = "none";
		document.getElementById("divPulsanti").style.display = "flex";
		document.getElementById("messaggio").innerHTML = "Ora crea la tabella";
	}, 1000);
}
var posizione = 1; 

// ---------
// GENERA TABELLA
// ---------
var avatar;
function generaTabella(){
	var imgPersonaggio = '<img src="img/imgDado/personaggi/' + personaggioSelezionato  + '_piccolo.png">'; 
	// togliere i messaggi precedenti
	// creare un avatar in alto a dx
	avatar = document.createElement("img");
	var divAvatar = document.getElementById("divAvatar");
	avatar.src = 'img/imgDado/personaggi/' + personaggioSelezionato  + '_piccolo.png';
	avatar.id = "avatar";
	divAvatar.appendChild(avatar);

    function disegna(posizione){
	    if (percorso[posizione] == BUCO){
		    return '<img src="img/imgDado/oca/p' + BUCO + '.jpg">';
			} else if (percorso[posizione] == SPINE){
			    return '<img src="img/imgDado/oca/p' + SPINE + '.jpg">';
			}else if(percorso[posizione] == FUNGHETTI){
			    return '<img src="img/imgDado/oca/p' + FUNGHETTI + '.jpg">';
			}else if(percorso[posizione] == COPPA){
				return '<img src="img/imgDado/oca/p' + COPPA + '.png">';
			}else{
				return i;
			}
    }	
	var s = ""; 
	var imgPersonaggio = '<img src="img/imgDado/personaggi/' + personaggioSelezionato  + '_piccolo.png">'; 
	s = s + "<tr>"; // creare prima riga
	s = s +  '<td id="p1">' + imgPersonaggio + '</td>'; // creare prima cella
					
	for (var i=2; i<=10; i++ ){
		s = s + '<td id="p' + i + '">' + disegna(i) + '</td>';
	}
	s = s + '</tr>';
					
	s = s + "<tr>"; 
	for (j=20; j>=11; j-- ){ 
	    s = s +  '<td id="p' + j + '">' + disegna(j) + '</td>';
	}
	s = s + '</tr>';
	s = s + "<tr>"; 
    for (var i=1; i<=10; i++ ){
	    var j = i + 20; 
		s = s +  '<td id="p' + j + '">' + disegna(j) + '</td>';
	}
	s = s + '</tr>';
	//  gradiente nelle celle
	document.getElementById("pianoGenerato").innerHTML = s;
	for(var k=1; k<=30; k++){
		document.getElementById("p" + k).style.backgroundColor = "rgb(" + red[0] + "," + red[1] + "," + red[2] + ")";
		red[0] = red[0] - differenza1;
		red[1] = red[1] + differenza2;
		red[2] = red[2] + differenza3;
		// document.getElementById("p" + k).style.borderTop = (k*2) + "px solid #F4ED01";
	}	
	document.getElementById("pianoGenerato").style.border = "3px solid black";
}
				
function lancia(){
	// generazione del numero casuale da 1 a 6:
	var n = Math.random();
	n = n * 6 + 1;  // numero tra 1 e 6.9999999999
	n = Math.floor(n);  // numero intero, prelevo solo la parte intera
	
	// elimino i messaggi delle mosse precedenti
	document.getElementById("messaggio").innerHTML = "";

	// mostrare glitch generale
	document.getElementById("dado").src = "img/imgDado/glitch/0.gif";

	// mostrare la faccia del dado aggiornata
	setTimeout(function mostraRisultatoDado(){
		document.getElementById("dado").src = "img/imgDado/glitch/dado_" + n + ".png";
	}, 500);
					
	// sposta l'oca dopo 1 secondo dal lancio del dado
	setTimeout(function mostraRisultatoDado(){
		muovi(n);
	}, 1000);
}

var mosseFatte = 0;		
var haiVinto = document.getElementById("messaggioVittoria");	
var haiPerso = document.getElementById("messaggioSconfitta");	

function muovi(numeroPassi){ 
	var imgPersonaggio = '<img src="img/imgDado/personaggi/' + personaggioSelezionato  + '_piccolo.png">'; 
	mosseFatte++;
	// mostrare il numero di mosse rimanenti prima della sconfitta
	document.getElementById("divMosseFatte").style.display = "block";
	document.getElementById("mosseFatte").innerHTML = (10 - mosseFatte);
	document.getElementById("mosseFatte").style.color = "green";
	if(mosseFatte >= 5){
		document.getElementById("mosseFatte").style.color = "orange";
	}
	if(mosseFatte >= 7){
		document.getElementById("mosseFatte").style.color = "red";
	}
    // nella posizione attuale devo rimettere il numero della cella
	// nel punto di arrivo devo disegnare l'oca
	var id = "p" + posizione; // p1
	document.getElementById(id).innerHTML = posizione; 
	
	// calcolo nuova posizione:
	posizione = posizione + numeroPassi;  
	document.getElementById("messaggio").innerHTML = "Ti sei mosso di " + numeroPassi + " caselle.";
	
	if (posizione > 30){
	    var posizioneOltre = posizione - 30;
		posizione = 30 - posizioneOltre;
		document.getElementById("messaggio").innerHTML = "Sei andato oltre la 30esima casella, quindi sei tornato indietro di " + posizioneOltre + " caselle.";

	}
	// vittoria
	if (posizione == 30){
		// togliere tutto
		document.getElementById("divControlli").style.display = "none"; 
		document.getElementById("pianoGenerato").style.display = "none"; 
		// riprodurre l'audio
		var audio = document.getElementById("audioVittoria");
		audio.play(); 
		// mostrare immagine grande dopo la vittoria
		var imgVittoria = document.createElement("img");
		var divTutto = document.getElementById("divTutto");
		divTutto.appendChild(imgVittoria);
		imgVittoria.id = "immagineVittoria";
		imgVittoria.src = "img/imgDado/vittoria/vittoria_" + personaggioSelezionato + ".gif";
		// mostrare messaggio vittoria
		haiVinto.style.display = "block";
	}
	// sconfitta
	if(mosseFatte == 10){
		// togliere tutto
		document.getElementById("divControlli").style.display = "none"; 
		document.getElementById("pianoGenerato").style.display = "none"; 
		document.getElementById("messaggio").style.display = "none"; 
		// mostrare immagine grande dopo la sconfitta
		var imgSconfitta = document.createElement("img");
		var divTutto = document.getElementById("divTutto");
		divTutto.appendChild(imgSconfitta);
		imgSconfitta.id = "immagineSconfitta";
		imgSconfitta.src = "img/imgDado/personaggi/" + personaggioSelezionato + ".png";
		// mostrare messaggio sconfitta
		haiPerso.style.display = "block";
		setTimeout(function (){
			imgSconfitta.src = "img/imgDado/sconfitta/sconfitta_" + personaggioSelezionato + ".gif";
			haiPerso.style.display = "none";
		}, 3000);
	}
	if(percorso[posizione] == BUCO){
	    posizione = 1;
		document.getElementById("messaggio").innerHTML = "Sei tornato al punto di partenza perchè sei andato dentro ad un buco nella casella " + posizione;
	}
	if(percorso[posizione] == SPINE){
	    posizione = posizione - 3;
	    document.getElementById("messaggio").innerHTML = "Sei tornato indietro di 3 caselle perchè ti sei ferito con le spine nella casella " + posizione;
	}
	if(percorso[posizione] == FUNGHETTI){
	    posizione = posizione + 5;
		document.getElementById("messaggio").innerHTML = "Sei andato avanti di 10 caselle perchè hai bevuto la pozione magica nella casella " + posizione;
	}
	// disegno l'oca nella nuova posizione
	id = "p" + posizione; 
	document.getElementById(id).innerHTML = imgPersonaggio; 
}