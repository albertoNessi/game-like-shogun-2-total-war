var i = 0;
var tutto = document.getElementById("divTutto");
var caricamento = document.getElementById("divCaricamento");
var avviso = document.getElementById("avvisoCaricamento");
var progresso = document.getElementById("mostraProgresso");
var barra = document.getElementById("myBar");

function anteprima() {
    tutto.style.display = "none";
    caricamento.style.display = "block";
    progresso.style.display = "block";
    if (i == 0) {
        i = 1;
        var width = 1;
        var aumentaProgressBar = setInterval(frame, 10);
        function frame() {
            // se il caricamento è finito...
            if (width >= 100) {
                clearInterval(aumentaProgressBar);
                i = 0;
                avviso.style.display = "block";
                avviso.style.color = "white";
            } else {
                width++;
                barra.style.width = width + "%";
                progresso.innerHTML = width  + "%";
            }
        }
    }
}
function anteprimaOut(){
    avviso.innerHTML = "Benvenuto!";
    barra.style.display = "none";
    progresso.style.display = "none";
    setTimeout(function(){ 
        caricamento.style.display = "none";
        tutto.style.display = "block";
    }, 1000);
}
function calcIn(){
    var calc = document.getElementById("index_divCalc");
    calc.src = "img/imgCalc/calc.gif";
}
function calcOut(){
    var calc = document.getElementById("index_divCalc");
    calc.src = "img/imgCalc/index_imgCalc_1.jpg";
}
function dadoIn(){
    var calc = document.getElementById("index_divDado");
    calc.src = "img/imgDado/dado_index/gifDado1.gif";
}
function dadoOut(){
    var calc = document.getElementById("index_divDado");
    calc.src = "img/imgDado/dado_index/gifDado1.jpg";
}