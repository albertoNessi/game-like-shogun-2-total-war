var R = 50; 
var C = 20; 

var CONFINI = ["0_1", "0_2", "0_3", "0_4", "0_5", "0_6", "0_7", "0_8", "1_0", "2_0", "3_0", "4_0", "5_0", "6_1", "6_2","6_3", "7_4", "6_6", "6_7", "5_8", "4_8", "3_9", "2_9", "1_10", "0_9", "8_4", "9_4", "10_4", "11_4", "7_6", "8_6", "9_6","10_6", "11_6", "12_4", "12_6", "11_7", "11_8", "13_9", "14_9", "15_10", "16_9", "17_8", "18_7", "19_6", "19_5", "19_4","18_3", "17_4", "15_4", "14_4", "13_4", "15_3", "16_3", "17_3", "15_19", "16_19", "17_18", "17_17", "17_16", "16_15","16_14", "17_13", "17_12", "16_11", "15_11", "14_12", "13_13", "13_14", "13_15", "13_16", "14_18", "10_9", "10_10","10_11", "10_12", "10_13", "9_14", "9_15", "9_16", "9_17", "10_18", "10_19", "10_20", "10_21", "11_22", "12_23", "12_24","12_10", "13_11", "13_12", "14_19", "11_25", "10_26", "10_27", "9_28", "8_29", "8_30", "9_31", "10_30", "10_31", "10_32","10_33", "9_34", "9_35", "9_36", "8_37", "15_20", "17_19", "18_20", "18_21", "18_22", "18_23", "18_24", "18_25", "17_26","18_27", "18_28", "18_29", "18_30", "18_31", "18_32", "18_33", "19_34", "19_35", "18_36", "18_37", "17_37", "16_37","15_38", "14_39", "13_40", "12_41", "11_41", "11_42", "11_43", "10_44", "7_38", "7_39", "6_40", "5_40", "4_40", "4_41","3_41", "2_42", "2_43", "1_44", "1_45", "1_46", "1_47", "2_48", "3_47", "4_47", "5_47", "6_47", "7_47", "8_47", "9_46","10_45", "12_14", "15_14", "11_34", "7_45"];

var COLORECONFINE = "rgb(161, 128, 78)";
var COLOREPERICOLO = "rgba(132, 7, 5, 0.5)";
var COLOREPERICOLOACCESO = "rgb(132, 7, 5)";
var COLORECITTACONQUISTATA = "rgba(46, 96, 134, 0.5)";

var pathImg = "img/imgGioco/";

var armataX = 1; 
var armataY = 1;

var divRapportoForze = document.getElementById("divRapportoForze");
var progressArmata = document.getElementById("progressArmata");
var progressNemici = document.getElementById("progressNemici");

function Armata(POSIZIONE, UNITA, DANNO){
	this.POSIZIONE = POSIZIONE;
	this.UNITA = UNITA;
	this.DANNO = DANNO;
}
var ARMATA = new Armata(
	armataX + "_" + armataY,
	100,
	10
)
function Citta(POSIZIONE, UNITA, VITAMURA, PERICOLO, DANNO) {
	this.POSIZIONE = POSIZIONE;
	this.UNITA = UNITA;
	this.VITAMURA = VITAMURA;
	this.PERICOLO = PERICOLO;
	this.DANNO = DANNO;
}
Citta.prototype.ATTACCA = function(){
	var dannoMura = Math.random();
	dannoMura = dannoMura * 5 + 1;
	dannoMura = Math.floor(dannoMura);
}
  
var CITTAVERDE = new Citta(
	"17_4",  
	30, 	 
	40, 	 
	["15_4", "15_5", "15_6", "16_4", "16_5", "16_6", "17_6", "17_5", "18_5", "18_4", "18_6"],	
	5	  
);
var CITTAROSSA = new Citta(
	"12_14",    
	20, 		
	40, 		
	["12_12", "11_12", "11_14", "11_15", "12_15", "12_16", "11_16", "10_14", "10_15", "12_13", "11_13"],
	3			
);
var CITTAMARRONE = new Citta(
	"15_14", 	
	40, 		
	60, 		
	["15_12", "15_13", "15_15", "15_16", "16_17", "16_16", "14_13", "14_14", "14_15", "14_16", "15_17", "15_18", "16_12", "16_13"],
	7			
);
var CITTAVIOLA = new Citta(
	"11_34", 	
	50, 		
	70, 		
	["11_32", "11_33", "11_36", "12_33", "12_34", "12_35", "11_35", "10_36", "10_35"],	
	9			
);
var CITTABLU = new Citta(
	"7_45", 	
	60, 		
	80, 		
	["8_43", "8_44", "7_43", "7_44", "6_44", "6_45", "6_46", "9_44", "9_43", "8_42", "7_42", "6_43", "5_44", "5_45", "5_46", "8_45", "7_46", "8_46"],
	11			
);

var PERICOLO = ["15_4", "15_5", "15_6", "16_4", "16_5", "16_6", "17_6", "17_5", "18_5", "18_4", "18_6", "12_12", "11_12","11_14", "11_15", "12_15", "12_16", "11_16", "10_14", "10_15", "12_13", "11_13", "15_12", "15_13", "15_15", "15_16","16_17", "16_16", "14_13", "14_14", "14_15", "14_16", "15_17", "15_18", "16_18", "16_12", "16_13", "11_32", "11_33","11_36", "12_33", "12_34", "12_35", "11_35", "10_36", "10_35", "8_43", "8_44", "7_43", "7_44", "6_44", "6_45", "6_46","9_44", "9_43", "8_42", "7_42", "6_43", "5_44", "5_45", "5_46", "8_45", "7_46", "8_46"];

var PONTE =  ["7_5", "8_5", "9_5", "10_5", "11_5", "12_9", "13_17", "14_17"];
var PONTEORIZZONTALE = "12_9";

var tabella = document.createElement("table");
var appendiQui = document.getElementById("divTabella");
appendiQui.appendChild(tabella);

for(var j=0; j<C; j++){
	var riga = document.createElement("tr");
	tabella.appendChild(riga);
	riga.id = "riga_" + j;
	for (var i=0; i<R; i++) {
		var imgCella = document.createElement("img");
		var cella = document.createElement("td");
		cella.appendChild(imgCella);
		riga.appendChild(cella);
		imgCella.src = pathImg + "0.png";
		imgCella.id = "imgCella_" + j + "_" + i;
		cella.id = "td_" + j + "_" + i;
	}
}
document.getElementById("imgCella_" + armataX + "_" + armataY).src = pathImg + "clan/clanPiccoli/armata.png";
document.getElementById("imgCella_" + CITTAVERDE.POSIZIONE).src = pathImg + "clan/clanPiccoli/cittaVerde.png";
document.getElementById("imgCella_" + CITTAROSSA.POSIZIONE).src = pathImg + "clan/clanPiccoli/cittaRossa.png";
document.getElementById("imgCella_" + CITTAMARRONE.POSIZIONE).src = pathImg + "clan/clanPiccoli/cittaMarrone.png";
document.getElementById("imgCella_" + CITTAVIOLA.POSIZIONE).src = pathImg + "clan/clanPiccoli/cittaViola.png";
document.getElementById("imgCella_" + CITTABLU.POSIZIONE).src = pathImg + "clan/clanPiccoli/cittaBlu.png";

for(var i=0; i<8; i++){
	document.getElementById("imgCella_" + PONTE[i]).src = "img/imgGioco/ponte.png";
}
document.getElementById("imgCella_" + PONTEORIZZONTALE).src = "img/imgGioco/ponte.png";
document.getElementById("imgCella_" + PONTEORIZZONTALE).style.transform = "rotate(90deg)";
