function checkKeyDown(e) {
	e = e || window.event;
	switch(e.keyCode){
		case 39: destra(); break;
		case 40: giu();    break;
		case 37: sinistra();   break;
		case 38: su();    break;
	}    
	// alert ("The Unicode character code is: " + e.keyCode);   
}
function checkKeyPress (event){
	var chCode = ('charCode' in event) ? event.charCode : event.keyCode;
	switch(chCode){
		case 100: destra();   break;
		case 115: giu();      break;
		case 97:  sinistra(); break;
		case 119: su();       break;
	}
	//alert ("The Unicode character code is: " + chCode);   
}
var nuovaPosizione;
function su(){
	var newX = (armataX -1 + C)%C;
	nuovaPosizione = newX + "_" + armataY;
	if(CONFINI.includes(nuovaPosizione)){
		return false;
	}else{
		sposta (armataX, armataY, newX, armataY);
	}
	if(PERICOLO.includes(nuovaPosizione)){
		document.getElementById("btnAttacco").style.display = "block";
	}else{
		document.getElementById("btnAttacco").style.display = "none";
	}
}
function sinistra(){
	var newY = (armataY -1 + R)%R; 
	nuovaPosizione = armataX + "_" + newY;
	if(CONFINI.includes(nuovaPosizione)){
		return false;
	}else{
		sposta (armataX, armataY, armataX, newY);
	}
	if(PERICOLO.includes(nuovaPosizione)){
		document.getElementById("btnAttacco").style.display = "block";
	}else{
		document.getElementById("btnAttacco").style.display = "none";
	}
}
function giu(){
	var newX = (armataX + 1 + C)%C;
	nuovaPosizione = newX + "_" + armataY;
	if(CONFINI.includes(nuovaPosizione)){
		return false;
	}else{ 
		sposta (armataX, armataY, newX, armataY);
	}
	if(PERICOLO.includes(nuovaPosizione)){
		document.getElementById("btnAttacco").style.display = "block";
	}else{
		document.getElementById("btnAttacco").style.display = "none";
	}
}
function destra(){
	var newY = (armataY + 1 + R)%R; 
	nuovaPosizione = armataX + "_" + newY;
	if(CONFINI.includes(nuovaPosizione)){
		return false;
	}else{ 
		sposta (armataX, armataY, armataX, newY);
	}
	if(PERICOLO.includes(nuovaPosizione)){
		document.getElementById("btnAttacco").style.display = "block";
	}else{
		document.getElementById("btnAttacco").style.display = "none";
	}
}
function sposta (daX, daY, aX, aY){
	var daSrc = "imgCella_" + daX + "_" + daY;  
	var posizionePartenza = daX + "_" + daY;
	
	armataX = aX;
	armataY = aY;
	ARMATA[0] = aX + "_" + aY;

	if(PONTE.includes(ARMATA[0]) && PONTE.includes(posizionePartenza)){
		document.getElementById(daSrc).src = pathImg + "ponte.png";
		document.getElementById("imgCella_" + aX + "_" + aY).src = pathImg + "clan/clanPiccoli/armata.png";
	}else if(PONTE.includes(posizionePartenza) == false){
		document.getElementById(daSrc).src = pathImg + "0.png";
		document.getElementById("imgCella_" + aX + "_" + aY).src = pathImg + "clan/clanPiccoli/armata.png";
	}else if(PONTE.includes(posizionePartenza)){
		document.getElementById(daSrc).src = pathImg + "ponte.png";
		document.getElementById("imgCella_" + aX + "_" + aY).src = pathImg + "clan/clanPiccoli/armata.png";
	}
}
function sfoca(){
	document.getElementById("nav").style.filter = "blur(10px)";
	document.getElementById("titolo").style.filter = "blur(10px)";
	document.getElementById("divComandiMappa").style.filter = "blur(10px)";
	document.getElementById("divTabella").style.filter = "blur(10px)";
}
function togliSfocatura(){
	document.getElementById("nav").style.filter = "none";
	document.getElementById("titolo").style.filter = "none";
	document.getElementById("divComandiMappa").style.filter = "none";
	document.getElementById("divTabella").style.filter = "none";
}
function mostraSpiegazioni(){
	document.getElementById("backgroundAssedio").style.display = "block";
	document.getElementById("spiegazione").style.display = "block";
	document.getElementById("titoloSpiegazione").style.display = "block";
	sfoca();
	creaImgSpiegazione();
}
function chiudiSpiegazione(){
	document.getElementById("backgroundAssedio").style.display = "none";
	document.getElementById("spiegazione").style.display = "none";
	document.getElementById("titoloSpiegazione").style.display = "none";
	document.getElementById("imgSpiegazione").style.display = "none";
	togliSfocatura();
	removeImgSpiegazione();
}
function chiudiBenvenuto(){
	document.getElementById("divBenvenuto").style.display = "none";
	document.getElementById("backgroundAssedio").style.display = "none";
	togliSfocatura();
}
function noSchizzi(){
	for(i=1; i<7; i++){
		document.getElementById("schizzo" + i).style.display = "none";
	}
}
function assedio(){
	document.getElementById("divAssedio").style.display = "block";
	document.getElementById("backgroundAssedio").style.display = "block";
	document.getElementById("divTabella").style.zIndex = "0";
	document.getElementById("divClanArmata").style.display = "block";
	document.getElementById("divClanNemico").style.display = "block";
	sfoca();
	creaImgSchizzi();
	creoImgClan();
	creaImgContestuale();
	
	if(CITTAVERDE.PERICOLO.includes(nuovaPosizione)){
		document.getElementById("btnIniziaAttacco").addEventListener("click", assedioVerde);
		document.getElementById("btnIniziaAttacco").style.display = "inline-block";
		
		document.getElementById("divMura").style.display = "block";
		document.getElementById("vitaMura").innerHTML = vitaMuraVerdi;
		
		document.getElementById("nomeClanNemico").innerHTML = "Clan Shimazu";
		document.getElementById("numNemici").innerHTML = numeroNemiciVerdi;
		document.getElementById("numArmata").innerHTML = numeroArmata;
		var tot = numeroArmata + numeroNemiciVerdi;
		var percentualeNemici = (numeroNemiciVerdi / tot) * 100;
		progressNemici.style.width = percentualeNemici + "%";
        var percentualeArmata = (numeroArmata / tot) * 100;
        progressArmata.style.width = percentualeArmata + "%";
	}
	if(CITTAROSSA.PERICOLO.includes(nuovaPosizione)){
		document.getElementById("btnIniziaAttacco").addEventListener("click", assedioRosso);
		document.getElementById("btnIniziaAttacco").style.display = "inline-block";
		document.getElementById("divMura").style.display = "block";
		document.getElementById("vitaMura").innerHTML = vitaMuraRosse;
		imgContestuale.src = "img/imgGioco/castelli/castello_2.jpg";
		document.getElementById("numNemici").innerHTML = numeroNemiciRossi;
		document.getElementById("numArmata").innerHTML = numeroArmata;
		document.getElementById("nomeClanNemico").innerHTML = "Clan Mori";
		
		document.getElementById("imgClanNemico").src = "img/imgGioco/clan/clanRosso.png";
		var tot = numeroArmata + numeroNemiciRossi;
		var percentualeNemici = (numeroNemiciRossi / tot) * 100;
		progressNemici.style.width = percentualeNemici + "%";
        var percentualeArmata = (numeroArmata / tot) * 100;
        progressArmata.style.width = percentualeArmata + "%";
	}
	if(CITTAMARRONE.PERICOLO.includes(nuovaPosizione)){
		document.getElementById("btnIniziaAttacco").addEventListener("click", assedioMarrone);
		document.getElementById("btnIniziaAttacco").style.display = "inline-block";
		
		document.getElementById("divMura").style.display = "block";
		document.getElementById("vitaMura").innerHTML = vitaMuraMarroni;
		
		imgContestuale.src = "img/imgGioco/castelli/castello_3.jpg";
		
		document.getElementById("numNemici").innerHTML = numeroNemiciMarroni;
		document.getElementById("numArmata").innerHTML = numeroArmata;
		document.getElementById("nomeClanNemico").innerHTML = "Clan Chosokabe";
		
		document.getElementById("imgClanNemico").src = "img/imgGioco/clan/clanMarrone.png";
		
		var tot = numeroArmata + numeroNemiciMarroni;
		var percentualeNemici = (numeroNemiciMarroni / tot) * 100;
		progressNemici.style.width = percentualeNemici + "%";
        var percentualeArmata = (numeroArmata / tot) * 100;
        progressArmata.style.width = percentualeArmata + "%";
	}
	if(CITTAVIOLA.PERICOLO.includes(nuovaPosizione)){
		document.getElementById("btnIniziaAttacco").addEventListener("click", assedioViola);
		document.getElementById("btnIniziaAttacco").style.display = "inline-block";
		
		document.getElementById("divMura").style.display = "block";
		document.getElementById("vitaMura").innerHTML = vitaMuraViola;
		
		imgContestuale.src = "img/imgGioco/castelli/castello.jpg";
		
		document.getElementById("numNemici").innerHTML = numeroNemiciViola;
		document.getElementById("vitaMura").innerHTML = vitaMuraViola;
		document.getElementById("numArmata").innerHTML = numeroArmata;
		document.getElementById("nomeClanNemico").innerHTML = "Clan Uesugi";
		
		document.getElementById("imgClanNemico").src = "img/imgGioco/clan/clanViola.png";
		
		var tot = numeroArmata + numeroNemiciViola;
		var percentualeNemici = (numeroNemiciViola / tot) * 100;
		progressNemici.style.width = percentualeNemici + "%";
        var percentualeArmata = (numeroArmata / tot) * 100;
        progressArmata.style.width = percentualeArmata + "%";
	}
	if(CITTABLU.PERICOLO.includes(nuovaPosizione)){
		document.getElementById("btnIniziaAttacco").addEventListener("click", assedioBlu);
		document.getElementById("btnIniziaAttacco").style.display = "inline-block";
		
		document.getElementById("divMura").style.display = "block";
		document.getElementById("vitaMura").innerHTML = vitaMuraBlu;
		
		imgContestuale.src = "img/imgGioco/castelli/castello_2.jpg";
		
		document.getElementById("numNemici").innerHTML = numeroNemiciBlu;
		document.getElementById("vitaMura").innerHTML = vitaMuraBlu;
		document.getElementById("numArmata").innerHTML = numeroArmata;
		document.getElementById("nomeClanNemico").innerHTML = "Clan Date";
		
		document.getElementById("imgClanNemico").src = "img/imgGioco/clan/clanBlu.png";
		
		var tot = numeroArmata + numeroNemiciBlu;
		var percentualeNemici = (numeroNemiciBlu / tot) * 100;
		progressNemici.style.width = percentualeNemici + "%";
        var percentualeArmata = (numeroArmata / tot) * 100;
        progressArmata.style.width = percentualeArmata + "%";
	}
}
	