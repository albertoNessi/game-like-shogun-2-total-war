function ricarica(){
    location.reload();
}
function creaBtnRiprova(){
    var btnRicarica = document.createElement("button");
    btnRicarica.innerHTML = "Riprova";
    btnRicarica.classList.add("btn");
    btnRicarica.style.margin = "10px auto";
    btnRicarica.style.textAlign = "center";
    btnRicarica.style.display = "block";
    btnRicarica.style.width = "100px";
    document.getElementById("divRisultati").appendChild(btnRicarica);
    btnRicarica.addEventListener("click", ricarica);   
}
function tolgoTutto(){
    divClanArmata.style.display = "none";
    document.getElementById("backgroundAssedio").style.display = "none";
    document.getElementById("divAssedio").style.display = "none";
    mostraUnitaArmata.innerHTML = "Unità nella tua armata: " + numeroArmata;
    mostraQui.style.display = "none";
    document.getElementById("divTabella").style.display = "none";
    document.getElementById("divComandiMappa").style.display = "none";
}
function sconfittaGenerale(){
    tolgoTutto();
    noSchizzi();
    document.getElementById("divRisultati").style.display = "block";
    creaImgSconfitta();
    document.getElementById("imgSconfittaGenerale").style.display = "block";
    document.getElementById("messaggioSconfittaGenerale").style.display = "block";
    creaBtnRiprova();
    removeImgClan();
}
function vittoriaGenerale(){
    tolgoTutto();
    noSchizzi();
    document.getElementById("divRisultati").style.display = "block";
    creaImgVittoria();
    document.getElementById("imgVittoriaGenerale").style.display = "block";
    document.getElementById("messaggioVittoriaGenerale").style.display = "block";
    creaBtnRiprova();
    removeImgClan();
}
